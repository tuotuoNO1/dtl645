#ifndef SERIALTHREAD_H
#define SERIALTHREAD_H
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QThread>


class SerialThread: public QObject
{
    Q_OBJECT;//Q_OBJECT宏，使程序可以用信号与槽机制
protected:


public:
    SerialThread();
    QSerialPort * simpleSerial;
    bool 串口打开标志=false;
    QList<int> comConj;
    QString comNo;
    int BaudTime;

signals:
    void openedCom(int 串口打开标志);//告诉其它窗口，串口是否已打开
    void sendReceiveData(QByteArray data);//将接收到的数据发到其它窗口中

public slots:
    void getConj(QString comNo,QList<int> comConj);
    void openConj(QString comNo,QList<int> comConj);
    void sendData(QByteArray data);//用以接收其它窗口传递过来的数据，再发送到串口

private slots:
    void ReadData();
    void waitRead();


};

#endif // SERIALTHREAD_H
