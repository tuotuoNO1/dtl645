#include "checkdataform.h"
#include "ui_checkdataform.h"
#include "qdebug.h"
#include "QString"
#include "convertool.h"


CheckDataForm::CheckDataForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CheckDataForm)
{
    ui->setupUi(this);
   // setAttribute(Qt::WA_DeleteOnClose);//  使Qt子窗体关闭时，也执行析构函数

    ui->combo_selectConcrol->setCurrentIndex(4);//13码，下拉菜单默认显示第2项
    控制码="13";
    ui->radio_verifySend->setChecked(false);//默认设置为false
    ui->radio_verifySend->setEnabled(false);//默认设置为false
    ui->push_send->setEnabled(false);//默认设置为false
    qDebug()<<"checkdataform线程"<<QThread::currentThread();

}

//使子窗口读取知悉串口状态
void CheckDataForm::comState(int 串口状态)
{
    if(串口状态==1){
        ui->push_send->setEnabled(true);
        ui->radio_verifySend->setEnabled(true);
        ui->radio_verifySend->setChecked(true);
    }

    else if(串口状态==2||串口状态==3){
        ui->push_send->setEnabled(false);
        ui->radio_verifySend->setEnabled(false);
        ui->radio_verifySend->setChecked(false);
    }
   qDebug()<<"checkdataform串口打开了吗"<<串口状态;
}

CheckDataForm::~CheckDataForm()
{

   // disconnect(this,&CheckDataForm::sendData2,SerThr,&SerialThread::sendData);//CheckDataForm界面发送数据
   // disconnect(SerThr,&SerialThread::sendReceiveData,this,&CheckDataForm::receiveData);//串口接收到数据，发送cdf窗口中。
    qDebug()<<"关闭了checkdataform";
    delete ui;
}





//下拉框选择控制码
void CheckDataForm::on_combo_selectConcrol_currentTextChanged(const QString &arg1)
{
    控制码=arg1.left(2);
    控制码的拼接模式(控制码);
}

//用来区分帧内的格式，用到的区域
void  CheckDataForm::控制码的拼接模式(QString 控制码)
{
    ui->lineE_2control->setText(控制码);
    QList<int> 拼接模式=DL645拼接模式(控制码);//根据控制码，得到拼接模式

    if(拼接模式[0]) {ui->lineE_4flag->setEnabled(true);}//标识符
             else {ui->lineE_4flag->setEnabled(false);}

    if(拼接模式[1]) {ui->lineE_5passwd->setEnabled(true);}//密码
             else {ui->lineE_5passwd->setEnabled(false);}

    if(拼接模式[2]) {ui->lineE_6operator->setEnabled(true);}//操作者代码
             else {ui->lineE_6operator->setEnabled(false);}

    if(拼接模式[3]) {ui->lineE_7dataArea->setEnabled(true);}//DATA区
             else {ui->lineE_7dataArea->setEnabled(false);}

    if(拼接模式[4]) {ui->lineE_SEQ->setEnabled(true);}//帧序号
             else {ui->lineE_SEQ->setEnabled(false);}
}

QByteArray CheckDataForm::生成发送帧()
{

    地址      =ui->lineE_1address->text();
    控制码    =ui->lineE_2control->text();
    //长度Str=ui->lineE_3length->text();
    标识符=ui->lineE_4flag->text();
    密码      =ui->lineE_5passwd->text();
    操作者代码 =ui->lineE_6operator->text();
    DATA区   =ui->lineE_7dataArea->text();
    帧序号    =ui->lineE_SEQ->text();

    QByteArray 待发送= 生成DL645帧(地址, 控制码,&长度Str,标识符,密码,操作者代码,DATA区,帧序号);
    //&长度Str使用指针，从调用函数中修改本身的值，再更新到界面中
    ui->lineE_3length->setText(长度Str);


    地址=过滤非HEX字符(地址);
    控制码=过滤非HEX字符(控制码);
    标识符=过滤非HEX字符(标识符);
    密码=过滤非HEX字符(密码);
    操作者代码=过滤非HEX字符(操作者代码);
    DATA区=过滤非HEX字符(DATA区);
    帧序号=过滤非HEX字符(帧序号);

    显示发送=地址+" "+控制码+" "+长度Str;//+" "+标识符+" "+密码+" "+操作者代码+" "+DATA区+" "+帧序号;

    return 待发送;
}

void CheckDataForm::on_push_verifySend_clicked()
{
    bool 是否校验并发送=ui->radio_verifySend->isChecked();
    控制码=ui->lineE_2control->text();
    QByteArray 待发送;

    待发送=生成发送帧();

    QList<int> 拼接模式=DL645拼接模式(控制码);
    if(拼接模式[0])显示发送+=" "+标识符;
    if(拼接模式[1])显示发送+=" "+密码;
    if(拼接模式[2])显示发送+=" "+操作者代码;
    if(拼接模式[3])显示发送+=" "+DATA区;
    if(拼接模式[4])显示发送+=" "+帧序号;


    QString 显示发送帧=QBtAy转HEX字符串(待发送,1);
    ui->lineE_sendArea->setText(显示发送帧);
    ui->textBr_commandMean->append("发送的帧："+显示发送帧);
    ui->textBr_commandMean->append("发送数据："+显示发送);//未加33H的，只有checkdataForm的控件中的值
    if(是否校验并发送)
    {
        qDebug()<<"cdf待发送"<<待发送.toHex();
        emit sendData2(待发送);
    }
}


//直接将发送帧内的数据发送到串口
void CheckDataForm::on_push_send_clicked()
{
    QByteArray 待发送;
    //四区=加33H(ui->lineE_4flag->text());
    待发送=HEX字符串转HEX数组(过滤非HEX字符(ui->lineE_sendArea->text()));
    ui->textBr_commandMean->append("发送的帧："+ui->lineE_sendArea->text());


    QString 地址,减33H后;
    // 验证接收帧(Sdata);
    QString 帧的解析=验证接收帧(待发送,&地址,&减33H后);
    ui->textBr_commandMean->append("发送数据："+地址+减33H后);
    emit sendData2(待发送);
}


//接收串口传递过来的数据
void CheckDataForm::receiveData(QByteArray Sdata)
{
    qDebug()<<"串口再发送到窗口的"<<Sdata;
    QString 接收的帧=QBtAy转HEX字符串(Sdata,1);
    QString 地址,减33H后;
    // 验证接收帧(Sdata);
    QString 帧的解析=验证接收帧(Sdata,&地址,&减33H后);
    //ui->textBr_commandMean->setText(接收的帧);

    ui->textBr_commandMean->append("接收的帧："+接收的帧);
    ui->textBr_commandMean->append("减33H后 ："+减33H后);
    ui->textBr_commandMean->append(帧的解析+'\n');
    ui->textBr_commandMean_2->append(帧的解析+'\n');//只显示解析后的含义
   // qDebug()<<"接收帧长度"<<Sdata.length()<<'\n';
}

//清空接收内容框
void CheckDataForm::on_pButt_clear_clicked()
{
    ui->textBr_commandMean->clear();
    ui->textBr_commandMean_2->clear();
}


void CheckDataForm::receiveMWsend(QString MW发送帧, QString 未加33H的)
{
    ui->textBr_commandMean->append("发送的帧："+MW发送帧);
    ui->textBr_commandMean->append("未加33前："+未加33H的);
}

void CheckDataForm::on_push_interpretReceive_clicked()
{
    QString 要解析帧 =ui->lineE_receiveArea->text();
    ui->textBr_commandMean->append("要解析帧："+要解析帧);


    要解析帧=过滤非HEX字符(要解析帧);
    QByteArray 中间QByAr;
    中间QByAr=HEX字符串转HEX数组(要解析帧);

    QString 地址,减33H后;
    QString 帧的解析=验证接收帧(中间QByAr,&地址,&减33H后);

    ui->textBr_commandMean->append("减33H后："+地址+" "+减33H后);
    ui->textBr_commandMean->append(帧的解析+'\n');
}

