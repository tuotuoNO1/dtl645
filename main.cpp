#include "mw.h"
#include <QApplication>
#include <QLocale>
#include <QTranslator>

int main(int argc, char *argv[])
{
//    //QT5需要用这句，QT6不用，因其自带高分屏适配
//    #if(QT_VERSION >= QT_VERSION_CHECK(5, 6, 0))//高分屏适配
       QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
//    #endif

    QApplication a(argc, argv);

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "DL645_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            a.installTranslator(&translator);
            break;
        }
    }
    MW w;
    w.show();

    return a.exec();
}
