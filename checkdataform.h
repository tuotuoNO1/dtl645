#ifndef CHECKDATAFORM_H
#define CHECKDATAFORM_H

#include <QWidget>
#include <QThread>
#include "serialthread.h"



namespace Ui {
class CheckDataForm;
}

class CheckDataForm : public QWidget
{
    Q_OBJECT

public:
    explicit CheckDataForm(QWidget *parent = nullptr);
    ~CheckDataForm();

    QByteArray 生成发送帧();
    void 控制码的拼接模式(QString 控制码);
    //QByteArray 一区,二区,三区,四区,五区,六区,七区,八区;
    QString 地址,控制码,长度Str,标识符,密码,操作者代码,DATA区,帧序号;
    QString 显示发送;

    SerialThread *SerThr;
    QThread *thread;

signals:
    void sendData2(QByteArray Sdata);//信号的函数名不能为中文

public slots:
    void receiveData(QByteArray Sdata);
    void comState(int 串口打开标志);//使子窗口读取知悉串口状态
    void receiveMWsend(QString MW发送的数据,QString 未加33H的);


private slots:
    void on_combo_selectConcrol_currentTextChanged(const QString &arg1);
    void on_push_verifySend_clicked();
    void on_push_send_clicked();

    void on_pButt_clear_clicked();

    void on_push_interpretReceive_clicked();

private:
    Ui::CheckDataForm *ui;
};


QByteArray 生成发送帧();


#endif // CHECKDATAFORM_H
