#include "serialthread.h"
#include <QDebug>
//#include <QMessageBox.h>
#include <QString>
#include<QTimer>
//#include<QTime>
//#include <synchapi.h>


SerialThread::SerialThread()
{
     qDebug()<<"线程开启了";
     串口打开标志=false;
     BaudTime=50;
     comConj<<0<<0<<0<<0<<0;

}


//getConj得到串口配置后再交由openConj打开串口
void SerialThread::getConj(QString comNo,QList<int> comConj)
{
    qDebug()<<"serial当前线程"<<QThread::currentThread();
    openConj(comNo,comConj);
}


void SerialThread::openConj(QString comNo,QList<int> comConj)
{
    int 串口状态=0;
    if(!串口打开标志)//如果串口本线程未打开
    {
        simpleSerial =new QSerialPort;
        simpleSerial->setReadBufferSize(0);//设置接收缓冲区大小，0是无穷大

        qDebug()<<"串口接收到的配置"<<comConj;

        simpleSerial->setPortName(comNo);
        if(simpleSerial->open(QIODevice::ReadWrite))//如果串口能打开
        {
            qDebug()<<"开始打开串口，已经打开";

//            comNo//设置串口号
//            comConj[0]//设置波特率
//            comConj[1]//设置校验位
//            comConj[2]/设置停止位
//            comConj[3]//设置数据位数
//            comConj[4]//设置流控制

            //设置波特率
            switch (comConj[0])
            {
               case 0:simpleSerial->setBaudRate(QSerialPort::Baud1200);BaudTime=640;break;
               case 1:simpleSerial->setBaudRate(QSerialPort::Baud2400);BaudTime=320;break;
               case 2:simpleSerial->setBaudRate(QSerialPort::Baud4800);BaudTime=160;break;
               case 3:simpleSerial->setBaudRate(QSerialPort::Baud9600);BaudTime=80;break;
               case 4:simpleSerial->setBaudRate(QSerialPort::Baud19200);BaudTime=40;break;//设置波特率为19200
               case 5:simpleSerial->setBaudRate(QSerialPort::Baud38400);BaudTime=30;break;
               case 6:simpleSerial->setBaudRate(QSerialPort::Baud57600);BaudTime=30;break;//只能设57600，不能强设56000；
               case 7:simpleSerial->setBaudRate(QSerialPort::Baud115200);BaudTime=30;break;
               default:break;
            }

            //设置校验位
            switch (comConj[1])
            {
               case 0: simpleSerial->setParity(QSerialPort::NoParity);break;//无校验none
               case 1: simpleSerial->setParity(QSerialPort::OddParity);break;//奇校验odd
               case 2: simpleSerial->setParity(QSerialPort::EvenParity);break;//偶校验even
               case 3: simpleSerial->setParity(QSerialPort::MarkParity);break;//为1,mark
               case 4: simpleSerial->setParity(QSerialPort::SpaceParity);break;//为0,space
               default: break;
            }

            //设置停止位
            switch (comConj[2])
            {
               case 0: simpleSerial->setStopBits(QSerialPort::OneStop); break;//停止位设置为1
               case 1: simpleSerial->setStopBits(QSerialPort::OneAndHalfStop); break;//停止位设置为1.5
               case 2: simpleSerial->setStopBits(QSerialPort::TwoStop); break;//停止位设置为2
               default:break;
            }

            //设置数据位数
            switch (comConj[3])
            {
               case 0:simpleSerial->setDataBits(QSerialPort::Data5);break;
               case 1:simpleSerial->setDataBits(QSerialPort::Data6);break;
               case 2:simpleSerial->setDataBits(QSerialPort::Data7);break;
               case 3:simpleSerial->setDataBits(QSerialPort::Data8);break;//设置数据位8
               default:break;

            }

            //设置流控制
            switch (comConj[4])
           {
               case 0: simpleSerial->setFlowControl(QSerialPort::NoFlowControl);break;//无流控制
               case 1: simpleSerial->setFlowControl(QSerialPort::HardwareControl);break;//硬件控制 (RTS/CTS)
               case 2: simpleSerial->setFlowControl(QSerialPort::SoftwareControl);break;//软件控制 (XON/XOFF)
               default:break;
           }
            串口打开标志=1;
            串口状态 =1;
            emit openedCom(串口状态);

            //连接读取串口数据的信号槽
            QObject::connect(simpleSerial,&QSerialPort::readyRead,this,&SerialThread::waitRead);
            //QObject::connect(simpleSerial,&QSerialPort::readyRead,this,&SerialThread::ReadData);

            qDebug()<<"serial当前线程"<<QThread::currentThread();
        }
        else{
            串口状态=3;
            emit openedCom(串口状态);
            simpleSerial->close();
            simpleSerial->deleteLater();
            qDebug()<<"串口无法打开";
           // QMessageBox::information(NULL, "提示", "串口无法打开");//不能serial线程中使用QMessageBox

        }
    }
//串口状态=0，未打开
//串口状态=1. 正常打开
//串口状态=2，已关闭
//串口状态=3，无法打开
    else
    {
        //关闭串口
        串口状态=2;
        串口打开标志=false;
        simpleSerial->clear();
        simpleSerial->close();
        simpleSerial->deleteLater();
        emit openedCom(串口状态);
        qDebug()<<"已关闭串口";
    }
}

//发送数据到串口
void SerialThread::sendData(QByteArray data)
{
   qDebug()<<"发送数据到串口"<<data;
   simpleSerial->write(data);//数据写入到串口中
}

//读取接收到的信息
void SerialThread::ReadData()
{
   // Sleep(20);
    QByteArray buf;
    buf = simpleSerial->readAll();

    if(!buf.isEmpty())//buf读到内容，才会上传数据到窗口,不保证能把数据完全读完
    {
       qDebug()<<"串口接收到数据"<<buf;
       emit sendReceiveData(buf);
    }

   // qDebug()<<"：串口读取完成";

}

//当&QSerialPort::readyRead发送信号说准备要开始读了，则执行此定时函数，不能用sleep()
//接CP2102,每个字节执行一次&QSerialPort::readyRead，这是问题
//设置读数的间隔，后续更改为根据波特率调为不同的间隔
void SerialThread::waitRead()
{
   // QDateTime qdt1 = QDateTime::currentDateTime();
   // QString timeStr = qdt1.toString("hhmmsszzz");
   // qDebug()<<timeStr<<"：串口读取完成";
     QTimer::singleShot(BaudTime, this,&SerialThread::ReadData);
}

