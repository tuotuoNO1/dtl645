#ifndef CONVERTOOL_H
#define CONVERTOOL_H
#include <QWidget>
#include <QSettings>

//class converTool
//{
//public:
//    converTool();
//};
QString 过滤非HEX字符(QString ss);
QByteArray 只逆序(QString hexStr);
QByteArray HEX字符串转HEX数组(QString hexStr);
//uchar 两HEX字符转1字节数(uchar a,uchar b);
quint8 两HEX字符转1字节数(quint8 a,quint8 b);

QString QBtAy转HEX字符串(QByteArray Str,int 是否有空格);
QByteArray 加33H(QString hexStr);
QByteArray 减33H(QByteArray hexByt);

int Write_CONFIG_INI(QString iniFile, QString section, QString key, QStringList value);//value原来是int型
QStringList Read_CONFIG_INI(QString iniFile, QString section, QString key);

QByteArray 生成DL645帧(QString 地址, QString 操作码,QString * 长度Str, QString 标识符,QString 密码,QString 操作者代码,QString DATA区,QString 帧序号);
QList<int> DL645拼接模式(QString 操作码);
QString 验证接收帧(QByteArray Sdata,QString *地址,QString * 减33H后);
QString 解释帧含义(QString 控制码,QString 标识符,QString 密码,QString 操作者代码,QString DATA区,QString 帧序号);
QString 两HexQStr字符ASCII(QString 两字符);
QString ERR码解析(QString 两字符);
QString 通用91码解帧方式(QStringList 解析方式,QString DATA区);

#endif // CONVERTOOL_H
