#ifndef MW_H
#define MW_H

#include <QMainWindow>
#include "serialthread.h"
#include <QThread>
#include "checkdataform.h"
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class MW; }
QT_END_NAMESPACE



class MW : public QMainWindow {
    Q_OBJECT

public:
    MW(QWidget *parent = nullptr);
    ~MW();
    SerialThread *SerThr;
    QThread *thread;

    QList<int> comConj;
    QString comNo;
    int 串口打开状态;//只用于其它子窗口读取知悉串口状态
    bool checkDataForm已创建;
    CheckDataForm *cdf= new CheckDataForm;//窗口设为公有在类中的每个函数都可操作

private:
    QStringList keys;//自动读取的键
    int 第几个键,未收到需等待,是否写入到table;
    QTimer *time1,*time2;
    QString ELadd;//塑壳地址


signals:
    void sendDataSignal(QByteArray Sdata);//信号的函数名不能为中文
    void sendConj(QString comNo,QList<int> comConj);
    void comState(int 串口打开状态);//使子窗口读取知悉串口状态
    //void mw_sendData(QByteArray Sdata);//信号的函数名不能为中文
    void sendMWdata(QString MW发送的数据,QString 未加33H的);//只用来给checkdataform的含义框显示数据

public slots:
    void sendDataSlot();//接收将要发送的数据，然后发给串口
    void receiveData(QByteArray Sdata);//接收串口返回的数据
    void SENDgetELdata();//接收查询塑壳信息，然后发给串口


private slots:
    void on_pushButton_clicked();
    void on_openSerial_clicked();
    void comOpened(int 串口状态);
    void on_pButt_645test_clicked();

    void on_pButt_start_clicked();

    void on_pButt_getELdata_clicked();

    void on_pButt_updateTime_clicked();

    void on_pushButton_2_clicked();

    void on_pButt_changeELadd_clicked();

    void on_pButt_clear_clicked();

    void on_pButt_selectINIfile_clicked();

private:
    Ui::MW *ui;

   // Ui::CheckDataForm *cdf;//设为私有，只有在一个函数中操作
    //如果不加这条语句，MW和CheckDataForm在同一线程，两个类也无法产生关联，
    //这里相当于把CheckDataForm当作MW的子窗口，关于CheckDataForm与子线程之间的操作都应放到mw.cpp中。
};
#endif // MW_H
