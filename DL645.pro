QT       += core gui
QT       += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    checkdataform.cpp \
    convertool.cpp \
    main.cpp \
    mw.cpp \
    serialthread.cpp

HEADERS += \
    checkdataform.h \
    convertool.h \
    mw.h \
    serialthread.h

FORMS += \
    checkdataform.ui \
    mw.ui

TRANSLATIONS += \
    DL645_zh_CN.ts
CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#用msvc编译，必须要用下列语句才能编译有中文的文件
win32-msvc* {
    QMAKE_CXXFLAGS += /source-charset:utf-8 /execution-charset:utf-8
}

DISTFILES += \
    发送区说明.txt \
    发送区说明.txt

RESOURCES += \
    ico.qrc

RC_ICONS = MandunDLT645.ico
